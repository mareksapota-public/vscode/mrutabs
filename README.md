# MRUTabs

This extension keeps your tabs/open editors list in most recently used order.

- The active editor is always first on the tab bar.
- The previously active editor is second on the tab bar.
- The second to last used editor is third on the tab bar.
- etc.

This extension can be useful if you often work with 4-5 files that you
frequently switch between.  Keepeing the tabs in most recently used order allows
you to switch between recently viewed files very easily.  If you like
meticulously ordering your tabs this is not an extension for you.

Pinned editors and preview editors are ignored and not reordered by this
extension.

## Example

Tab 1 is active, if you click on tab 3 it will be moved to the first position
and other tabs will be pushed to the right.

```
 active               click
    ↓                   ↓
┌───────┐ ┌───────┐ ┌───────┐
│   1   │ │   2   │ │   3   │
├───────┴─┴───────┴─┴───────┴──────────────┐
│                                          │
│   contents of tab 1                      │


 active   old active
    ↓         ↓
┌───────┐ ┌───────┐ ┌───────┐
│   3   │ │   1   │ │   2   │
├───────┴─┴───────┴─┴───────┴──────────────┐
│                                          │
│   contents of tab 3                      │
```


The selected tab 3 is now active and has been moved to the first position.  If
you now click on tab 1 it will be moved to the first position and the currently
active tab 3 will move to the second position as the previously opened editor.

```
 active     click
    ↓         ↓
┌───────┐ ┌───────┐ ┌───────┐
│   3   │ │   1   │ │   2   │
├───────┴─┴───────┴─┴───────┴──────────────┐
│                                          │
│   contents of tab 3                      │


 active   old active
    ↓         ↓
┌───────┐ ┌───────┐ ┌───────┐
│   1   │ │   3   │ │   2   │
├───────┴─┴───────┴─┴───────┴──────────────┐
│                                          │
│   contents of tab 1                      │
```

Clicking on tab 2 will move it again to the first position and push other tabs
to the right.

```
 active               click
    ↓                   ↓
┌───────┐ ┌───────┐ ┌───────┐
│   1   │ │   3   │ │   2   │
├───────┴─┴───────┴─┴───────┴──────────────┐
│                                          │
│   contents of tab 1                      │


 active   old active
    ↓         ↓
┌───────┐ ┌───────┐ ┌───────┐
│   2   │ │   1   │ │   3   │
├───────┴─┴───────┴─┴───────┴──────────────┐
│                                          │
│   contents of tab 2                      │
```

Tab 2 is active and at the first position, previously opened tab 1 is on the
second position and the second to last opened tab 3 is on the third position.
