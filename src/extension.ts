"use strict";

// MIT Expat License

// Copyright 2016-2022 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import * as vscode from "vscode";

export function activate() {
  function countPinnedTabs(tabs: readonly vscode.Tab[]): number {
    return tabs.filter((tab) => tab.isPinned).length;
  }
  function moveTabToFront(event: vscode.TabChangeEvent) {
    const activeTabs = event.changed.filter((tab) => tab.isActive);
    // Unclear if there can ever be more than 1 active tab.
    activeTabs.forEach((activeTab) => {
      if (activeTab.isPinned || activeTab.isPreview) {
        // Do not move preview or pinned tabs.
        return;
      }
      if (activeTab.group.tabs[0] === activeTab) {
        // Nothing to do.  Avoid self triggering after the tab has been
        // moved.
        return;
      }
      // See documentation at
      // https://github.com/microsoft/vscode/blob/964dc545abd63c5f6f6c42755f1c11c2c912a0b5/src/vs/workbench/browser/parts/editor/editorCommands.ts#L97
      vscode.commands.executeCommand("moveActiveEditor", {
        to: "position",
        // Indexes start at 1, skip any pinned tabs
        value: countPinnedTabs(activeTab.group.tabs) + 1,
      });
    });
  }
  vscode.window.tabGroups.onDidChangeTabs(moveTabToFront);
}

export function deactivate() {}
