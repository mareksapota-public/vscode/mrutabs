"use strict";

// MIT Expat License

// Copyright 2016-2022 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import * as assert from "assert";
import { beforeEach } from "mocha";
import * as path from "path";

import * as vscode from "vscode";

const sleep = (ms: number) => new Promise((resolve) => setTimeout(resolve, ms));
const TIMEOUT = 500;
const SLEEP_TIME = 10;

async function openFile(
  fileName: string,
  { preview = false } = {},
): Promise<vscode.TextDocument> {
  // rootPath is deprecated but workspaceFolders is undefined so it's not an
  // option
  const rootPath = vscode.workspace.rootPath;
  if (rootPath === undefined) {
    throw new Error("Expected root path to be set");
  }
  const filePath = path.join(rootPath, fileName);
  const document = await vscode.workspace.openTextDocument(filePath);
  await vscode.window.showTextDocument(document, { preview: preview });
  return document;
}

function getTabLabels(): string[] {
  return vscode.window.tabGroups.activeTabGroup.tabs.map((tab) => tab.label);
}

function checkTabLabels(expectedLabels: readonly string[]): boolean {
  const actualLabels = getTabLabels();
  if (actualLabels.length !== expectedLabels.length) {
    return false;
  }
  return actualLabels.every(
    (actual, index) => actual === expectedLabels[index],
  );
}

async function assertTabLabels(
  expectedLabels: readonly string[],
): Promise<void> {
  // Extension will not work instantly, tests have to wait for the tabs to
  // switch.
  const startTime = Date.now();
  let currentTime = startTime;
  while (currentTime <= startTime + TIMEOUT) {
    if (checkTabLabels(expectedLabels)) {
      break;
    }
    await sleep(SLEEP_TIME);
    currentTime = Date.now();
  }
  const actualLabels = getTabLabels();
  assert.deepStrictEqual(
    actualLabels,
    expectedLabels,
    `Actual: ${actualLabels} Expected: ${expectedLabels}`,
  );
}

const FILE1 = "file1";
const FILE2 = "file2";
const FILE3 = "file3";
const FILE4 = "file4";

suite("Extension Test Suite", () => {
  beforeEach(async () => {
    await vscode.commands.executeCommand("workbench.action.closeAllEditors");
    while (vscode.window.tabGroups.activeTabGroup.tabs.length > 0) {
      await vscode.commands.executeCommand("workbench.action.unpinEditor");
      await vscode.commands.executeCommand(
        "workbench.action.closeActiveEditor",
      );
    }
  });

  test("Open files", async () => {
    await openFile(FILE1);
    await assertTabLabels([FILE1]);
    await openFile(FILE2);
    await assertTabLabels([FILE2, FILE1]);
    await openFile(FILE3);
    await assertTabLabels([FILE3, FILE2, FILE1]);
    await openFile(FILE4);
    await assertTabLabels([FILE4, FILE3, FILE2, FILE1]);
  });

  test("Change active editor", async () => {
    const file4 = await openFile(FILE4);
    await openFile(FILE3);
    await openFile(FILE2);
    const file1 = await openFile(FILE1);
    await assertTabLabels([FILE1, FILE2, FILE3, FILE4]);

    vscode.window.showTextDocument(file4);
    await assertTabLabels([FILE4, FILE1, FILE2, FILE3]);
    vscode.window.showTextDocument(file1);
    await assertTabLabels([FILE1, FILE4, FILE2, FILE3]);
  });

  test("Close active editor", async () => {
    await openFile(FILE4);
    await openFile(FILE3);
    await openFile(FILE2);
    await openFile(FILE1);
    await assertTabLabels([FILE1, FILE2, FILE3, FILE4]);

    await vscode.commands.executeCommand("workbench.action.closeActiveEditor");
    await assertTabLabels([FILE2, FILE3, FILE4]);
    await vscode.commands.executeCommand("workbench.action.closeActiveEditor");
    await assertTabLabels([FILE3, FILE4]);
  });

  test("Pinned editors", async () => {
    const file4 = await openFile(FILE4);
    const file3 = await openFile(FILE3);
    const file2 = await openFile(FILE2);
    await openFile(FILE1);
    await assertTabLabels([FILE1, FILE2, FILE3, FILE4]);

    // pin file1
    await vscode.commands.executeCommand("workbench.action.pinEditor");
    await assertTabLabels([FILE1, FILE2, FILE3, FILE4]);
    // pin file2
    await vscode.window.showTextDocument(file2);
    await vscode.commands.executeCommand("workbench.action.pinEditor");
    await assertTabLabels([FILE1, FILE2, FILE3, FILE4]);

    await vscode.window.showTextDocument(file3);
    await assertTabLabels([FILE1, FILE2, FILE3, FILE4]);
    await vscode.window.showTextDocument(file4);
    await assertTabLabels([FILE1, FILE2, FILE4, FILE3]);
  });

  test("Pinned editors 2", async () => {
    const file4 = await openFile(FILE4);
    const file3 = await openFile(FILE3);
    await openFile(FILE2);
    await openFile(FILE1);
    await assertTabLabels([FILE1, FILE2, FILE3, FILE4]);

    // pin file4
    await vscode.window.showTextDocument(file4);
    await vscode.commands.executeCommand("workbench.action.pinEditor");
    await assertTabLabels([FILE4, FILE1, FILE2, FILE3]);

    await vscode.window.showTextDocument(file3);
    await assertTabLabels([FILE4, FILE3, FILE1, FILE2]);
  });

  test("Preview editors", async () => {
    const file2 = await openFile(FILE2);
    await openFile(FILE1);
    await assertTabLabels([FILE1, FILE2]);

    await openFile(FILE3, { preview: true });
    await assertTabLabels([FILE1, FILE3, FILE2]);
    await vscode.window.showTextDocument(file2);
    await assertTabLabels([FILE2, FILE1, FILE3]);
    await openFile(FILE4, { preview: true });
    await assertTabLabels([FILE2, FILE4, FILE1]);
    await openFile(FILE3, { preview: true });
    await assertTabLabels([FILE2, FILE3, FILE1]);
  });
});
